<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
ini_set("error_reporting",E_ALL ^ E_NOTICE);

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'XWeather',
	'import'=>array(
        'application.models.*',
        'application.components.*',
    ),
	// application components
	'components'=>array(

		// uncomment the following to use a MySQL database
		'db'=>array(
            'class'=>'CDbConnectionExt',
            'connectionString' => 'mysql:host=dbserver_weather_write;port=3306;dbname=x_weather',
            'emulatePrepare' => true,
            'username' => 'dbuser_weather',
            'password' => '123456',
            'charset' => 'utf8',
            'tablePrefix'=>'w_',
            'slaveConfig'=>array(
                array('connectionString'=>'mysql:host=dbserver_weather_read;dbname=x_weather;port=3306','username'=>'dbuser_weather','password'=>'123456'),
            ),
        ),
        
        'curl'=>array(
            'class'=>'application.extensions.curl.Curl',
              //eg.
               'options'=>array(
                    'timeout'=>0,
                    'setOptions'=>array(
                        CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows NT 6.1; rv:6.0.2) Gecko/20100101 Firefox/6.0.2',
                        CURLOPT_REFERER   => 'http://m.weather.com.cn/m/pn12/weather.htm',
                        CURLOPT_COOKIE    => '  vjuids=4ebc7d510.1361008174f.0.cd982c576d47d8; vjlast=1331708827.1333079826.11',
                    ),
                ),
        ),
	),
);